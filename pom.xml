<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.mte.numecoeval</groupId>
        <artifactId>core</artifactId>
        <version>1.0.0</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>
    <artifactId>api-event-enrichissement</artifactId>
    <name>api-event-enrichissement</name>
    <version>1.0.0</version>
    <description>api-event-enrichissement</description>

    <repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab-forge.din.developpement-durable.gouv.fr/api/v4/groups/5389/-/packages/maven</url>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
        </snapshotRepository>
    </distributionManagement>

    <dependencies>
        <dependency>
            <groupId>org.mte.numecoeval</groupId>
            <artifactId>common</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mte.numecoeval</groupId>
            <artifactId>dto-asyncapi</artifactId>
            <version>1.0.0</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-integration</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-cache</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.integration</groupId>
            <artifactId>spring-integration-kafka</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.kafka</groupId>
            <artifactId>spring-kafka</artifactId>
        </dependency>

        <!-- Mapping -->
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct</artifactId>
        </dependency>

        <!-- Utilitaire -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
        </dependency>

        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <scope>runtime</scope>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>io.micrometer</groupId>
            <artifactId>micrometer-registry-prometheus</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.integration</groupId>
            <artifactId>spring-integration-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.kafka</groupId>
            <artifactId>spring-kafka-test</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Base de données de tests -->
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Données de tests -->
        <dependency>
            <groupId>org.instancio</groupId>
            <artifactId>instancio-junit</artifactId>
            <scope>test</scope>
        </dependency>

        <!-- Mock de serveur -->
        <dependency>
            <groupId>com.github.tomakehurst</groupId>
            <artifactId>wiremock-jre8-standalone</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- Tests avec attente minimum -->
        <dependency>
            <groupId>org.awaitility</groupId>
            <artifactId>awaitility</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                        </exclude>
                    </excludes>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>${openapi-generator-version}</version>
                <executions>
                    <execution>
                        <id>generation_client_referentiel</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${project.basedir}/src/main/resources/static/openapi-referentiels.yaml</inputSpec>
                            <output>${project.build.directory}/generated-sources</output>
                            <modelPackage>org.mte.numecoeval.referentiel.generated.api.model</modelPackage>
                            <apiPackage>org.mte.numecoeval.referentiel.generated.api.client</apiPackage>
                            <invokerPackage>org.mte.numecoeval.referentiel.generated.api.invoker</invokerPackage>
                            <generateApis>true</generateApis>
                            <generateModels>true</generateModels>
                            <generateModelTests>false</generateModelTests>
                            <generateApiTests>false</generateApiTests>
                            <generateSupportingFiles>true</generateSupportingFiles>
                            <generateModelDocumentation>false</generateModelDocumentation>
                            <generateApiDocumentation>false</generateApiDocumentation>
                            <generatorName>java</generatorName>
                            <library>webclient</library>
                            <configOptions>
                                <useJakartaEe>true</useJakartaEe>
                                <useTags>true</useTags>
                                <skipDefaultInterface>true</skipDefaultInterface>
                                <dateLibrary>java8</dateLibrary>
                                <useSpringBoot3>true</useSpringBoot3>
                                <sourceFolder>src/gen/java</sourceFolder>
                                <serializableModel>true</serializableModel>
                                <interfaceOnly>true</interfaceOnly>
                                <reactive>false</reactive>
                                <useBeanValidation>true</useBeanValidation>
                                <performBeanValidation>true</performBeanValidation>
                                <useOptional>false</useOptional>
                                <serviceInterface>true</serviceInterface>
                                <serviceImplementation>true</serviceImplementation>
                                <booleanGetterPrefix>is</booleanGetterPrefix>
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>

            </plugin>
        </plugins>
    </build>


</project>
