package org.mte.numecoeval.enrichissement.test.kafka.consommateur;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerMessageCalculMessagerieDTO extends KafkaConsumer<MessageCalculMessagerieDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.calcul.messagerie}")
    public void consumeIndicateurs(MessageCalculMessagerieDTO indicateurs) {
        log.info("#############  Playload = {}",indicateurs);
        payloads.add(indicateurs);
        latch.countDown();
    }
}
