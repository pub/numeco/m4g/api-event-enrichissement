package org.mte.numecoeval.enrichissement.test.kafka.consommateur;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@Slf4j
public abstract class KafkaConsumer<T> {
    List<T> payloads = new ArrayList<>();
    protected CountDownLatch latch = new CountDownLatch(1);

    abstract public void consumeIndicateurs(T indicateurs);

    public  void reset() {
        payloads = new ArrayList<>();
        resetLatch();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public void resetLatch() {
        latch = new CountDownLatch(1);
    }

    public List<T> getPayloads() {
        return payloads;
    }
}
