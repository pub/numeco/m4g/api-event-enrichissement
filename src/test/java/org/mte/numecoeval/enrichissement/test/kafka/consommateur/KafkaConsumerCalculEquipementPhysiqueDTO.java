package org.mte.numecoeval.enrichissement.test.kafka.consommateur;

import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
public class KafkaConsumerCalculEquipementPhysiqueDTO extends KafkaConsumer<CalculEquipementPhysiqueDTO> {

    @Override
    @KafkaListener(topics = "${numecoeval.topic.calcul.equipementPhysique}")
    public void consumeIndicateurs(CalculEquipementPhysiqueDTO indicateurs) {
        log.info("#############  Playload = {}",indicateurs);
        payloads.add(indicateurs);
        latch.countDown();
    }
}
