package org.mte.numecoeval.enrichissement.test.kafka.consommateur;

import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;

@Configuration
@EnableKafka
public class KafkaConsumerConfig {


    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapAddress;

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CalculEquipementPhysiqueDTO> kafkaListenerContainerFactoryCalculEquipementPhysiqueDTO(ConsumerFactory<String, CalculEquipementPhysiqueDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, CalculEquipementPhysiqueDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<CalculEquipementPhysiqueDTO> kafkaConsumerCalculEquipementPhysiqueDTO(){
        return new KafkaConsumerCalculEquipementPhysiqueDTO();
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, MessageCalculMessagerieDTO> kafkaListenerContainerFactoryMessageCalculMessagerieDTO(ConsumerFactory<String, MessageCalculMessagerieDTO> consumerFactory) {
        ConcurrentKafkaListenerContainerFactory<String, MessageCalculMessagerieDTO> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);
        return factory;
    }

    @Bean
    public KafkaConsumer<MessageCalculMessagerieDTO> kafkaConsumerMessageCalculMessagerieDTO(){
        return new KafkaConsumerMessageCalculMessagerieDTO();
    }
}
