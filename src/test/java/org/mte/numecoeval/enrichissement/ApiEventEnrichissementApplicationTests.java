package org.mte.numecoeval.enrichissement;

import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.enrichissement.factory.TestDataFactory;
import org.mte.numecoeval.enrichissement.infrastructure.config.CacheConfig;
import org.mte.numecoeval.enrichissement.test.kafka.consommateur.KafkaConsumerCalculEquipementPhysiqueDTO;
import org.mte.numecoeval.enrichissement.test.kafka.consommateur.KafkaConsumerConfig;
import org.mte.numecoeval.enrichissement.test.kafka.consommateur.KafkaConsumerMessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ContextConfiguration(
        classes = {ApiEventEnrichissementApplication.class, KafkaConsumerConfig.class}
)
@ActiveProfiles(profiles = { "test" })
@WireMockTest(httpPort =29090 )
@EmbeddedKafka(
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1
)
class ApiEventEnrichissementApplicationTests {

    @Autowired
    KafkaTemplate<String, EquipementPhysiqueDTO> kafkaTemplateEquipementPhysiqueDTO;

    @Autowired
    KafkaTemplate<String, MessagerieDTO> kafkaTemplateMessagerieDTO;

    @Autowired
    CacheConfig cacheConfig;

    @Autowired
    KafkaConsumerCalculEquipementPhysiqueDTO kafkaConsumerCalculEquipementPhysiqueDTO;

    @Autowired
    KafkaConsumerMessageCalculMessagerieDTO kafkaConsumerMessageCalculMessagerieDTO;

    @Value("${numecoeval.topic.entree.equipementPhysique}")
    String topicEntreeEquipementPhysique;

    @Value("${numecoeval.topic.entree.messagerie}")
    String topicEntreeMessagerie;

    @BeforeEach
    void setup() {
        cacheConfig.emptyAllCaches();
        kafkaConsumerCalculEquipementPhysiqueDTO.reset();
        kafkaConsumerMessageCalculMessagerieDTO.reset();
    }

    @Test
    void withValidInputForEquipementPhysique_shouldSendMessageToTopicCalcul(WireMockRuntimeInfo wmRuntimeInfo) {
        // Given
        assertNotNull(wmRuntimeInfo);
        wmRuntimeInfo.getWireMock().resetRequests();
        assertNotNull(kafkaConsumerCalculEquipementPhysiqueDTO);
        kafkaConsumerCalculEquipementPhysiqueDTO.reset();
        var modele = "laptop";
        var type = "Laptop";
        String refEquipementCible = "test";
        var pays = "France";
        var paysDataCenter = "Monaco";
        var equipementPhysique = Instancio.of(EquipementPhysiqueDTO.class).create();
        equipementPhysique.setModele(modele);
        equipementPhysique.setType(type);
        equipementPhysique.setPaysDUtilisation(pays);
        equipementPhysique.getDataCenter().setLocalisation(paysDataCenter);

        // Mocks
        TestDataFactory.baseMock(wmRuntimeInfo);
        TestDataFactory.mockCorrespondance(wmRuntimeInfo, modele, refEquipementCible);
        TestDataFactory.mockTypeEquipement(wmRuntimeInfo, type, refEquipementCible, 5.0);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, refEquipementCible, 50.0,12.12);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, "laptop", 150.0,12.12);
        TestDataFactory.mockAllRefImpactReseau(wmRuntimeInfo, "impactReseauMobileMoyen", 0.150,12.12);
        TestDataFactory.mockAllMixElectrique(wmRuntimeInfo, "France", 100.0);
        TestDataFactory.mockAllMixElectrique(wmRuntimeInfo, "Monaco", 125.0);

        // When
        var sendResultCompletableFuture = kafkaTemplateEquipementPhysiqueDTO.send(topicEntreeEquipementPhysique, equipementPhysique);

        await().atMost(10, TimeUnit.SECONDS)
                .until(()-> kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().size() == 1);
        // Then
        assertNotNull(sendResultCompletableFuture);
        var result = sendResultCompletableFuture.join();
        assertNotNull(result);
        assertTrue(sendResultCompletableFuture.isDone());

        var payload = kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().get(0);
        assertNotNull(payload);
        assertNotNull(payload.getCorrespondanceRefEquipement());
        assertNotNull(payload.getTypeEquipement());
        assertEquals(TestDataFactory.etapesACV().size(), payload.getEtapes().size());
        assertEquals(TestDataFactory.criteres().size(), payload.getCriteres().size());
        // 2 Hypothèses
        assertEquals(2, payload.getHypotheses().size());
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "dureeVieParDefaut".equals(hypotheseDTO.getCode())));
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "PUEParDefaut".equals(hypotheseDTO.getCode())));
        // Impact Equipement = nombre de critères * nombre d'étapes * nombre de références d'équipement différentes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size() * 2, payload.getImpactsEquipement().size());
        // Impact Rseeau = nombre de critères * nombre d'étapes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), payload.getImpactsReseau().size());
        // Mix Electrique = nombre de critères * nombre de pays différent
        assertEquals(TestDataFactory.criteres().size() * 2, payload.getMixElectriques().size());
    }

    @Test
    void whenTypeEquipementUnavailableAndCorrespondanceAvailable_shouldSendMessageToTopicCalcul(WireMockRuntimeInfo wmRuntimeInfo) {
        // Given
        assertNotNull(wmRuntimeInfo);
        wmRuntimeInfo.getWireMock().resetRequests();
        assertNotNull(kafkaConsumerCalculEquipementPhysiqueDTO);
        kafkaConsumerCalculEquipementPhysiqueDTO.reset();
        var modele = "laptop";
        var type = "Laptop";
        String refEquipementCible = "test";
        var pays = "France";
        var paysDataCenter = "France";
        var equipementPhysique = Instancio.of(EquipementPhysiqueDTO.class).create();
        equipementPhysique.setModele(modele);
        equipementPhysique.setType(type);
        equipementPhysique.setPaysDUtilisation(pays);
        equipementPhysique.getDataCenter().setLocalisation(paysDataCenter);

        // Mocks
        TestDataFactory.baseMock(wmRuntimeInfo);
        TestDataFactory.mockCorrespondance(wmRuntimeInfo, modele, refEquipementCible);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, refEquipementCible, 50.0,12.12);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, "laptop", 150.0,12.12);
        TestDataFactory.mockAllRefImpactReseau(wmRuntimeInfo, "impactReseauMobileMoyen", 0.150,12.12);
        TestDataFactory.mockAllMixElectrique(wmRuntimeInfo, "France", 100.0);

        // When
        var sendResultCompletableFuture = kafkaTemplateEquipementPhysiqueDTO.send(topicEntreeEquipementPhysique, equipementPhysique);

        await().atMost(10, TimeUnit.SECONDS)
                .until(()-> kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().size() == 1);
        // Then
        assertNotNull(sendResultCompletableFuture);
        var result = sendResultCompletableFuture.join();
        assertNotNull(result);
        assertTrue(sendResultCompletableFuture.isDone());

        var payload = kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().get(0);
        assertNotNull(payload);
        assertNotNull(payload.getCorrespondanceRefEquipement());
        assertNull(payload.getTypeEquipement());
        assertEquals(TestDataFactory.etapesACV().size(), payload.getEtapes().size());
        assertEquals(TestDataFactory.criteres().size(), payload.getCriteres().size());
        // 2 Hypothèses
        assertEquals(2, payload.getHypotheses().size());
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "dureeVieParDefaut".equals(hypotheseDTO.getCode())));
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "PUEParDefaut".equals(hypotheseDTO.getCode())));
        // Impact Equipement = nombre de critères * nombre d'étapes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), payload.getImpactsEquipement().size());
        // Impact Rseeau = nombre de critères * nombre d'étapes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), payload.getImpactsReseau().size());
        // Mix Electrique = nombre de critères
        assertEquals(TestDataFactory.criteres().size(), payload.getMixElectriques().size());
    }

    @Test
    void whenTypeEquipementAvailableAndCorrespondanceUnavailable_shouldSendMessageToTopicCalcul(WireMockRuntimeInfo wmRuntimeInfo) {
        // Given
        assertNotNull(wmRuntimeInfo);
        wmRuntimeInfo.getWireMock().resetRequests();
        assertNotNull(kafkaConsumerCalculEquipementPhysiqueDTO);
        kafkaConsumerCalculEquipementPhysiqueDTO.reset();
        var modele = "laptop";
        var type = "Laptop";
        String refEquipementCible = "test";
        var pays = "France";
        var paysDataCenter = "France";
        var equipementPhysique = Instancio.of(EquipementPhysiqueDTO.class).create();
        equipementPhysique.setModele(modele);
        equipementPhysique.setType(type);
        equipementPhysique.setPaysDUtilisation(pays);
        equipementPhysique.getDataCenter().setLocalisation(paysDataCenter);

        // Mocks
        TestDataFactory.baseMock(wmRuntimeInfo);
        TestDataFactory.mockTypeEquipement(wmRuntimeInfo, type, refEquipementCible, 5.0);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, refEquipementCible, 50.0,12.12);
        TestDataFactory.mockAllRefImpactEquipement(wmRuntimeInfo, "laptop", 150.0,12.12);
        TestDataFactory.mockAllRefImpactReseau(wmRuntimeInfo, "impactReseauMobileMoyen", 0.150,12.12);
        TestDataFactory.mockAllMixElectrique(wmRuntimeInfo, "France", 100.0);

        // When
        var sendResultCompletableFuture = kafkaTemplateEquipementPhysiqueDTO.send(topicEntreeEquipementPhysique, equipementPhysique);

        await().atMost(10, TimeUnit.SECONDS)
                .until(()-> kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().size() == 1);
        // Then
        assertNotNull(sendResultCompletableFuture);
        var result = sendResultCompletableFuture.join();
        assertNotNull(result);
        assertTrue(sendResultCompletableFuture.isDone());

        var payload = kafkaConsumerCalculEquipementPhysiqueDTO.getPayloads().get(0);
        assertNotNull(payload);
        assertNull(payload.getCorrespondanceRefEquipement());
        assertNotNull(payload.getTypeEquipement());
        assertEquals(TestDataFactory.etapesACV().size(), payload.getEtapes().size());
        assertEquals(TestDataFactory.criteres().size(), payload.getCriteres().size());
        // 2 Hypothèses
        assertEquals(2, payload.getHypotheses().size());
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "dureeVieParDefaut".equals(hypotheseDTO.getCode())));
        assertTrue(payload.getHypotheses().stream().anyMatch(hypotheseDTO -> "PUEParDefaut".equals(hypotheseDTO.getCode())));
        // Impact Equipement = nombre de critères * nombre d'étapes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), payload.getImpactsEquipement().size());
        // Impact Rseeau = nombre de critères * nombre d'étapes
        assertEquals(TestDataFactory.criteres().size() * TestDataFactory.etapesACV().size(), payload.getImpactsReseau().size());
        // Mix Electrique = nombre de critères
        assertEquals(TestDataFactory.criteres().size(), payload.getMixElectriques().size());
    }

    @Test
    void withValidInputForMessagerie_shouldSendMessageToTopicCalcul(WireMockRuntimeInfo wmRuntimeInfo) {
        // Given
        assertNotNull(wmRuntimeInfo);
        wmRuntimeInfo.getWireMock().resetRequests();
        assertNotNull(kafkaConsumerMessageCalculMessagerieDTO);
        kafkaConsumerMessageCalculMessagerieDTO.reset();
        var messagerieDTO = Instancio.of(MessagerieDTO.class).create();

        // Mocks
        TestDataFactory.baseMock(wmRuntimeInfo);
        TestDataFactory.mockAllImpactMessagerie(wmRuntimeInfo, 0.175, 0.00123);

        // When
        var sendResultCompletableFuture = kafkaTemplateMessagerieDTO.send(topicEntreeMessagerie, messagerieDTO);

        await().atMost(10, TimeUnit.SECONDS)
                .until(()-> kafkaConsumerMessageCalculMessagerieDTO.getPayloads().size() == 1);
        // Then
        assertNotNull(sendResultCompletableFuture);
        var result = sendResultCompletableFuture.join();
        assertNotNull(result);
        assertTrue(sendResultCompletableFuture.isDone());

        var payload = kafkaConsumerMessageCalculMessagerieDTO.getPayloads().get(0);
        assertNotNull(payload);
        assertNotNull(payload.getMessagerie());
        assertNotNull(payload.getCriteres());
        assertEquals(TestDataFactory.criteres().size(), payload.getCriteres().size());
        // Impact Messagerie = nombre de critères
        assertEquals(TestDataFactory.criteres().size(), payload.getImpactsMessagerie().size());
    }

    @Test
    void whenOnlyOneImpactMessagerieAvailable_shouldSendMessageToTopicCalcul(WireMockRuntimeInfo wmRuntimeInfo) {
        // Given
        assertNotNull(wmRuntimeInfo);
        wmRuntimeInfo.getWireMock().resetRequests();
        assertNotNull(kafkaConsumerMessageCalculMessagerieDTO);
        kafkaConsumerMessageCalculMessagerieDTO.reset();
        var messagerieDTO = Instancio.of(MessagerieDTO.class).create();

        // Mocks
        TestDataFactory.baseMock(wmRuntimeInfo);
        TestDataFactory.mockImpactMessagerie(wmRuntimeInfo, TestDataFactory.criteres().get(0).getNomCritere(), 0.175, 0.00123);

        // When
        var sendResultCompletableFuture = kafkaTemplateMessagerieDTO.send(topicEntreeMessagerie, messagerieDTO);

        await().atMost(10, TimeUnit.SECONDS)
                .until(()-> kafkaConsumerMessageCalculMessagerieDTO.getPayloads().size() == 1);
        // Then
        assertNotNull(sendResultCompletableFuture);
        var result = sendResultCompletableFuture.join();
        assertNotNull(result);
        assertTrue(sendResultCompletableFuture.isDone());

        var payload = kafkaConsumerMessageCalculMessagerieDTO.getPayloads().get(0);
        assertNotNull(payload);
        assertNotNull(payload.getMessagerie());
        assertNotNull(payload.getCriteres());
        assertEquals(TestDataFactory.criteres().size(), payload.getCriteres().size());
        // Impact Messagerie = nombre de critères
        assertEquals(1, payload.getImpactsMessagerie().size());
    }
}
