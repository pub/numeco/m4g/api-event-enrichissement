package org.mte.numecoeval.enrichissement.factory;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import org.mte.numecoeval.referentiel.generated.api.model.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.referentiel.generated.api.model.CritereDTO;
import org.mte.numecoeval.referentiel.generated.api.model.EtapeDTO;
import org.mte.numecoeval.referentiel.generated.api.model.HypotheseDTO;
import org.mte.numecoeval.referentiel.generated.api.model.ImpactEquipementDTO;
import org.mte.numecoeval.referentiel.generated.api.model.ImpactMessagerieDTO;
import org.mte.numecoeval.referentiel.generated.api.model.TypeEquipementDTO;
import org.mte.numecoeval.topic.data.ImpactReseauDTO;
import org.mte.numecoeval.topic.data.MixElectriqueDTO;
import wiremock.com.google.common.net.PercentEscaper;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

public class TestDataFactory {

    public static List<EtapeDTO> etapesACV() {
        return Arrays.asList(
                new EtapeDTO().code("FABRICATION").libelle("Fabrication"),
                new EtapeDTO().code("UTILISATION").libelle("Utilisation"),
                new EtapeDTO().code("FIN_DE_VIE").libelle("Fin de vie"),
                new EtapeDTO().code("DISTRIBUTION").libelle("Distribution")
        );
    }

    public static List<CritereDTO> criteres() {
        return Arrays.asList(
                new CritereDTO().nomCritere("Changement climatique").unite("kg CO_{2} eq"),
                new CritereDTO().nomCritere("Émissions de particules fines").unite("Diseaseincidence"),
                new CritereDTO().nomCritere("Radiations ionisantes").unite("kBq U-235 eq"),
                new CritereDTO().nomCritere("Acidification").unite("mol H^{+} eq"),
                new CritereDTO().nomCritere("Épuisement des ressources naturelles (minérales et métaux)").unite("kg Sb eq")
        );
    }

    public static List<TypeEquipementDTO> typesEquipement() {
        return Arrays.asList(
                new TypeEquipementDTO().type("Laptop").refEquipementParDefaut("laptop").dureeVieDefaut(3.0),
                new TypeEquipementDTO().type("Desktop").refEquipementParDefaut("desktop").dureeVieDefaut(5.0),
                new TypeEquipementDTO().type("Server").refEquipementParDefaut("server").dureeVieDefaut(6.0),
                new TypeEquipementDTO().type("Screen").refEquipementParDefaut("screen").dureeVieDefaut(8.0)
        );
    }

    public static void baseMock(WireMockRuntimeInfo wmRuntimeInfo) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get("/referentiel/etapes")
                .willReturn(ResponseDefinitionBuilder.okForJson(etapesACV())));

        server.register(WireMock.get("/referentiel/criteres")
                .willReturn(ResponseDefinitionBuilder.okForJson(criteres())));

        mockHypothese(wmRuntimeInfo, "dureeVieParDefaut", "6.0");
        mockHypothese(wmRuntimeInfo, "PUEParDefaut", "1.0");
    }

    public static void mockCorrespondance(WireMockRuntimeInfo wmRuntimeInfo, String modele, String refEquipementCible) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/correspondanceRefEquipement"))
                        .withQueryParam("modele", new EqualToPattern(modele))
                .willReturn(ResponseDefinitionBuilder.okForJson(new CorrespondanceRefEquipementDTO().modeleEquipementSource(modele).refEquipementCible(refEquipementCible))));

    }

    public static void mockHypothese(WireMockRuntimeInfo wmRuntimeInfo, String code, String valeurHypothese) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/hypothese"))
                .withQueryParam("cle", new EqualToPattern(code))
                .willReturn(ResponseDefinitionBuilder.okForJson(new HypotheseDTO().code(code).valeur(valeurHypothese))));

    }

    public static void mockRefImpactEquipement(WireMockRuntimeInfo wmRuntimeInfo, String critere, String etapeacv, String refEquipement, Double valeur, Double consoElecMoyenne) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/impactequipements"))
                        .withQueryParam("refEquipement", new EqualToPattern(refEquipement))
                        .withQueryParam("critere", new EqualToPattern(critere))
                        .withQueryParam("etapeacv", new EqualToPattern(etapeacv))
                .willReturn(ResponseDefinitionBuilder.okForJson(
                        new ImpactEquipementDTO().refEquipement(refEquipement)
                                .etape(etapeacv)
                                .critere(critere)
                                .valeur(valeur)
                                .consoElecMoyenne(consoElecMoyenne)
                        )
                )
        );
    }

    public static void mockAllRefImpactEquipement(WireMockRuntimeInfo wmRuntimeInfo, String refEquipement, Double valeur, Double consoElecMoyenne) {
        etapesACV().forEach(etapeDTO ->
                criteres().forEach(critereDTO -> {
                    mockRefImpactEquipement(wmRuntimeInfo, critereDTO.getNomCritere(), etapeDTO.getCode(), refEquipement, valeur, consoElecMoyenne);
                })
        );
    }

    public static void mockRefImpactReseau(WireMockRuntimeInfo wmRuntimeInfo, String critere, String etapeacv, String refReseau, Double valeur, Double consoElecMoyenne) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/impactreseaux"))
                        .withQueryParam("refReseau", new EqualToPattern(refReseau))
                        .withQueryParam("critere", new EqualToPattern(critere))
                        .withQueryParam("etapeacv", new EqualToPattern(etapeacv))
                .willReturn(ResponseDefinitionBuilder.okForJson(
                        new ImpactReseauDTO().refReseau(refReseau)
                                .etapeACV(etapeacv)
                                .critere(critere)
                                .valeur(valeur)
                                .consoElecMoyenne(consoElecMoyenne)
                        )
                )
        );
    }

    public static void mockAllRefImpactReseau(WireMockRuntimeInfo wmRuntimeInfo, String refEquipement, Double valeur, Double consoElecMoyenne) {
        etapesACV().forEach(etapeDTO ->
                criteres().forEach(critereDTO -> {
                    mockRefImpactReseau(wmRuntimeInfo, critereDTO.getNomCritere(), etapeDTO.getCode(), refEquipement, valeur, consoElecMoyenne);
                })
        );
    }

    public static void mockTypeEquipement(WireMockRuntimeInfo wmRuntimeInfo, String type, String refParDefaut, Double dureeDeVieParDefaut) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/typesEquipement/"+ URLEncoder.encode(type, StandardCharsets.UTF_8)))
                .willReturn(ResponseDefinitionBuilder.okForJson(
                        new TypeEquipementDTO().type(type).refEquipementParDefaut(refParDefaut).dureeVieDefaut(dureeDeVieParDefaut)
                ))
        );
    }

    public static void mockMixElectrique(WireMockRuntimeInfo wmRuntimeInfo, String critere, String pays, Double valeur) {

        WireMock server = wmRuntimeInfo.getWireMock();

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/mixelecs"))
                .withQueryParam("critere", new EqualToPattern(critere))
                .withQueryParam("pays", new EqualToPattern(pays))
                .willReturn(ResponseDefinitionBuilder.okForJson(
                                new MixElectriqueDTO()
                                        .critere(critere)
                                        .valeur(valeur)
                        )
                )
        );
    }


    public static void mockAllMixElectrique(WireMockRuntimeInfo wmRuntimeInfo, String pays, Double valeur) {
        criteres().forEach(critereDTO -> {
            mockMixElectrique(wmRuntimeInfo, critereDTO.getNomCritere(), pays, valeur);
        });
    }

    public static void mockImpactMessagerie(WireMockRuntimeInfo wmRuntimeInfo, String critere, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine) {

        WireMock server = wmRuntimeInfo.getWireMock();
        var escaper = new PercentEscaper("-_.*", false);

        server.register(WireMock.get(WireMock.urlPathEqualTo("/referentiel/impactsMessagerie/"+ escaper.escape(critere)))
                .willReturn(ResponseDefinitionBuilder.okForJson(
                                new ImpactMessagerieDTO()
                                        .critere(critere)
                                        .constanteCoefficientDirecteur(constanteCoefficientDirecteur)
                                        .constanteOrdonneeOrigine(constanteOrdonneeOrigine)
                        )
                )
        );
    }

    public static void mockAllImpactMessagerie(WireMockRuntimeInfo wmRuntimeInfo, Double constanteCoefficientDirecteur, Double constanteOrdonneeOrigine) {
        criteres().forEach(critereDTO -> {
            mockImpactMessagerie(wmRuntimeInfo, critereDTO.getNomCritere(), constanteCoefficientDirecteur, constanteOrdonneeOrigine);
        });
    }
}
