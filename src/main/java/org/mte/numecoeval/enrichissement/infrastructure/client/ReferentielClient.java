package org.mte.numecoeval.enrichissement.infrastructure.client;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.enrichissement.infrastructure.mapper.ReferentielToDTOMapper;
import org.mte.numecoeval.topic.data.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.HypotheseDTO;
import org.mte.numecoeval.topic.data.ImpactEquipementDTO;
import org.mte.numecoeval.topic.data.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.ImpactReseauDTO;
import org.mte.numecoeval.topic.data.MixElectriqueDTO;
import org.mte.numecoeval.topic.data.TypeEquipementDTO;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class ReferentielClient {

    private org.mte.numecoeval.referentiel.generated.api.client.InterneNumEcoEvalApi interneNumEcoEvalApi;

    private ReferentielToDTOMapper mapper;

    @Cacheable("Etapes")
    public List<EtapeDTO> getEtapes() {
        try {
            var restDtos = interneNumEcoEvalApi.getAllEtapes().collectList().block();
            return mapper.toEtapeDTO(restDtos);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /etapes", e);
        }
        return Collections.emptyList();
    }

    @Cacheable("Criteres")
    public List<CritereDTO> getCriteres() {
        try {
            var restDtos = interneNumEcoEvalApi.getAllCriteres().collectList().block();
            return mapper.toCritereDTO(restDtos);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /criteres", e);
        }
        return Collections.emptyList();
    }

    @Cacheable(value = "Hypothese", key = "T(org.apache.commons.lang3.StringUtils).defaultString(#code)")
    public HypotheseDTO getHypothese(String code) {
        try {
            var restDto = interneNumEcoEvalApi.getHypothese(code).block();
            return mapper.toHypotheseDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /hypotheses : code : {}", code, e);
        }
        return null;
    }

    @Cacheable(value = "CorrespondanceRefEquipement", key = "T(org.apache.commons.lang3.StringUtils).defaultString(#modele)")
    public CorrespondanceRefEquipementDTO getCorrespondanceRefEquipement(String modele) {
        try {
            var restDto = interneNumEcoEvalApi.getCorrespondanceRefEquipement(modele).block();
            return mapper.toCorrespondanceRefEquipementDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /correspondanceRefEquipement : modele : {}", modele, e);
        }
        return null;
    }

    @Cacheable(value = "TypeEquipement", key = "T(org.apache.commons.lang3.StringUtils).defaultString(#type)")
    public TypeEquipementDTO getTypeEquipement(String type) {
        try {
            var restDtos = interneNumEcoEvalApi.getTypeEquipement(type).block();
            return mapper.toTypeEquipementDTO(restDtos);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /typesEquipement : type : {}", type, e);
        }
        return null;
    }

    @Cacheable(value = "ImpactEquipement")
    public ImpactEquipementDTO getImpactEquipement(String refEquipement, String critere, String etape) {
        try {
            var restDto = interneNumEcoEvalApi.getImpactEquipement(refEquipement, critere, etape).block();
            return mapper.toImpactEquipementDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /impactequipements : refEquipement : {}, critere: {}, etape: {}", refEquipement, critere, etape, e);
        }
        return null;
    }

    @Cacheable(value = "ImpactReseau")
    public ImpactReseauDTO getImpactReseau(String refReseau, String critere, String etape) {
        try {
            var restDto = interneNumEcoEvalApi.getImpactReseau(refReseau, critere, etape).block();
            return mapper.toImpactReseauDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /impactreseaux : refReseau : {}, critere: {}, etape: {}", refReseau, critere, etape, e);
        }
        return null;
    }

    @Cacheable(value = "MixElectrique")
    public MixElectriqueDTO getMixElectrique(String pays, String critere) {
        try {
            var restDto = interneNumEcoEvalApi.getMixElectrique(pays, critere).block();
            return mapper.toMixElectriqueDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /mixelecs : pays : {}, critere: {}", pays, critere, e);
        }
        return null;
    }

    @Cacheable(value = "ImpactMessagerie", key = "T(org.apache.commons.lang3.StringUtils).defaultString(#critere)")
    public ImpactMessagerieDTO getMessagerie(String critere) {
        try {
            var restDto = interneNumEcoEvalApi.getImpactMessagerie(critere).block();
            return mapper.toImpactMessagerieDTO(restDto);
        }
        catch (Exception e) {
            log.error("Erreur lors de l'appel à l'API Référentiels : /referentiel/impactsMessagerie/{critere} : critere : {}", critere, e);
        }
        return null;
    }
}
