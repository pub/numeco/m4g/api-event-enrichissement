package org.mte.numecoeval.enrichissement.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.mte.numecoeval.enrichissement.infrastructure.client.ReferentielClient;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.HypotheseDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.eqp"
)
@Service
@AllArgsConstructor
public class EnrichissementEquipementPhysiqueService {

    private ReferentielClient referentielClient;

    @ServiceActivator(
            inputChannel = "equipementPhysiqueEnrichissement",
            outputChannel = "equipementPhysiqueOutputChannelToCalcul"
    )
    public CalculEquipementPhysiqueDTO serviceEnrichissementEquipementPhysique(EquipementPhysiqueDTO equipementPhysiqueDTO) {
        if(Objects.isNull(equipementPhysiqueDTO)) {
            return null;
        }
        log.info("Enrichissement d'un équipement physique : Nom Equipement: {}, Type: {}, Modele: {}",
                equipementPhysiqueDTO.getNomEquipementPhysique(), equipementPhysiqueDTO.getType(), equipementPhysiqueDTO.getModele());

        CalculEquipementPhysiqueDTO calculEquipementPhysiqueDTO = new CalculEquipementPhysiqueDTO();
        calculEquipementPhysiqueDTO.setEquipementPhysique(equipementPhysiqueDTO);

        calculEquipementPhysiqueDTO.setEtapes(referentielClient.getEtapes());
        calculEquipementPhysiqueDTO.setCriteres(referentielClient.getCriteres());
        var hypotheses = new ArrayList<HypotheseDTO>();
        hypotheses.add(referentielClient.getHypothese("dureeVieParDefaut"));
        hypotheses.add(referentielClient.getHypothese("PUEParDefaut"));
        calculEquipementPhysiqueDTO.setHypotheses(hypotheses);
        calculEquipementPhysiqueDTO.setCorrespondanceRefEquipement(referentielClient.getCorrespondanceRefEquipement(equipementPhysiqueDTO.getModele()));
        calculEquipementPhysiqueDTO.setTypeEquipement(referentielClient.getTypeEquipement(equipementPhysiqueDTO.getType()));

        calculEquipementPhysiqueDTO.setImpactsEquipement(new ArrayList<>());
        calculEquipementPhysiqueDTO.setImpactsReseau(new ArrayList<>());
        calculEquipementPhysiqueDTO.setMixElectriques(new ArrayList<>());
        for(var critere : ListUtils.emptyIfNull(calculEquipementPhysiqueDTO.getCriteres())) {
            for(var etape : ListUtils.emptyIfNull(calculEquipementPhysiqueDTO.getEtapes())) {
                if(calculEquipementPhysiqueDTO.getCorrespondanceRefEquipement() != null) {
                    calculEquipementPhysiqueDTO.getImpactsEquipement().add(
                            referentielClient.getImpactEquipement(
                                    calculEquipementPhysiqueDTO.getCorrespondanceRefEquipement().getRefEquipementCible(),
                                    critere.getNomCritere(),
                                    etape.getCode()
                            )
                    );
                }
                if(calculEquipementPhysiqueDTO.getTypeEquipement() != null
                        && StringUtils.isNotBlank(calculEquipementPhysiqueDTO.getTypeEquipement().getRefEquipementParDefaut())) {
                    calculEquipementPhysiqueDTO.getImpactsEquipement().add(
                            referentielClient.getImpactEquipement(
                                    calculEquipementPhysiqueDTO.getTypeEquipement().getRefEquipementParDefaut(),
                                    critere.getNomCritere(),
                                    etape.getCode()
                            )
                    );
                }
                calculEquipementPhysiqueDTO.getImpactsReseau().add(
                        referentielClient.getImpactReseau(
                                "impactReseauMobileMoyen",
                                critere.getNomCritere(),
                                etape.getCode()
                        )
                );
            }
            calculEquipementPhysiqueDTO.getMixElectriques().add(referentielClient.getMixElectrique(equipementPhysiqueDTO.getPaysDUtilisation(), critere.getNomCritere()));
            if(equipementPhysiqueDTO.getDataCenter() != null
                    && StringUtils.isNotBlank(equipementPhysiqueDTO.getDataCenter().getLocalisation())
                    && !StringUtils.equals(equipementPhysiqueDTO.getPaysDUtilisation(), equipementPhysiqueDTO.getDataCenter().getLocalisation())) {
                calculEquipementPhysiqueDTO.getMixElectriques().add(referentielClient.getMixElectrique(equipementPhysiqueDTO.getDataCenter().getLocalisation(), critere.getNomCritere()));
            }
        }

        return calculEquipementPhysiqueDTO;
    }
}
