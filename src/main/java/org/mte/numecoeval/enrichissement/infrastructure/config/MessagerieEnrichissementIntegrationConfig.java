package org.mte.numecoeval.enrichissement.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.mes"
)
public class MessagerieEnrichissementIntegrationConfig {

    @Value("${numecoeval.topic.entree.messagerie}")
    String topicEntreeMessagerie;

    @Value("${numecoeval.topic.calcul.messagerie}")
    String topicCalculMessagerie;

    @Bean
    public NewTopic messagerieTopicKafkaEntree() {
        return new NewTopic(topicEntreeMessagerie, 1, (short) 1);
    }

    @Bean
    public NewTopic messagerieTopicKafkaSortieCalcul() {
        return new NewTopic(topicCalculMessagerie, 1, (short) 1);
    }

    @Bean
    public MessageChannel messagerieEnrichissementChannel() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel messagerieOutputChannelToCalculChannel() {
        return new DirectChannel();
    }

    // Entrée
    @Bean
    public KafkaMessageDrivenChannelAdapter<String, MessagerieDTO> messagerieKafkaAdapter(KafkaMessageListenerContainer<String, MessagerieDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("messagerieEnrichissementChannel");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, MessagerieDTO> messagerieKafkaContainer(ConsumerFactory<String, MessagerieDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(this.topicEntreeMessagerie);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // Traitement voir classe EnrichissementMessagerieService

    // Sortie
    @Bean
    @ServiceActivator(inputChannel = "messagerieOutputChannelToCalculChannel")
    public MessageHandler messagerieCalculHandler(KafkaTemplate<String, MessageCalculMessagerieDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, MessageCalculMessagerieDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicCalculMessagerie));
        return handler;
    }

}
