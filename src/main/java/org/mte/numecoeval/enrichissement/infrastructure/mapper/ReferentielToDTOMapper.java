package org.mte.numecoeval.enrichissement.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mte.numecoeval.topic.data.CorrespondanceRefEquipementDTO;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.EtapeDTO;
import org.mte.numecoeval.topic.data.HypotheseDTO;
import org.mte.numecoeval.topic.data.ImpactEquipementDTO;
import org.mte.numecoeval.topic.data.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.ImpactReseauDTO;
import org.mte.numecoeval.topic.data.MixElectriqueDTO;
import org.mte.numecoeval.topic.data.TypeEquipementDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReferentielToDTOMapper {

    EtapeDTO toEtapeDTO(org.mte.numecoeval.referentiel.generated.api.model.EtapeDTO restDto);

    List<EtapeDTO> toEtapeDTO(List<org.mte.numecoeval.referentiel.generated.api.model.EtapeDTO> restDtos);

    CritereDTO toCritereDTO(org.mte.numecoeval.referentiel.generated.api.model.CritereDTO restDto);

    List<CritereDTO> toCritereDTO(List<org.mte.numecoeval.referentiel.generated.api.model.CritereDTO> restDtos);

    TypeEquipementDTO toTypeEquipementDTO(org.mte.numecoeval.referentiel.generated.api.model.TypeEquipementDTO restDto);

    HypotheseDTO toHypotheseDTO(org.mte.numecoeval.referentiel.generated.api.model.HypotheseDTO restDto);

    CorrespondanceRefEquipementDTO toCorrespondanceRefEquipementDTO(org.mte.numecoeval.referentiel.generated.api.model.CorrespondanceRefEquipementDTO restDto);

    MixElectriqueDTO toMixElectriqueDTO(org.mte.numecoeval.referentiel.generated.api.model.MixElectriqueDTO restDto);

    ImpactReseauDTO toImpactReseauDTO(org.mte.numecoeval.referentiel.generated.api.model.ImpactReseauDTO restDto);

    ImpactEquipementDTO toImpactEquipementDTO(org.mte.numecoeval.referentiel.generated.api.model.ImpactEquipementDTO restDto);

    ImpactMessagerieDTO toImpactMessagerieDTO(org.mte.numecoeval.referentiel.generated.api.model.ImpactMessagerieDTO restDto);

}
