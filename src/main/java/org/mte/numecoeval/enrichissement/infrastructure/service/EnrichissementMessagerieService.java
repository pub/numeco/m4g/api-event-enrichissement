package org.mte.numecoeval.enrichissement.infrastructure.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mte.numecoeval.enrichissement.infrastructure.client.ReferentielClient;
import org.mte.numecoeval.topic.data.CritereDTO;
import org.mte.numecoeval.topic.data.ImpactMessagerieDTO;
import org.mte.numecoeval.topic.data.MessageCalculMessagerieDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Objects;

@Slf4j
@ConditionalOnProperty(
        value = "numecoeval.features.mes"
)
@Service
@AllArgsConstructor
public class EnrichissementMessagerieService {

    private ReferentielClient referentielClient;

    @ServiceActivator(
            inputChannel = "messagerieEnrichissementChannel",
            outputChannel = "messagerieOutputChannelToCalculChannel"
    )
    public MessageCalculMessagerieDTO serviceEnrichissementMessagerie(MessagerieDTO messagerieDTO) {
        if(Objects.isNull(messagerieDTO)) {
            return null;
        }
        log.info("Enrichissement d'un élément de messagerie : Mois-Années: {}",
                messagerieDTO.getMoisAnnee());

        MessageCalculMessagerieDTO calculMessagerieDTO = new MessageCalculMessagerieDTO();
        calculMessagerieDTO.setMessagerie(messagerieDTO);

        calculMessagerieDTO.setCriteres(referentielClient.getCriteres());
        calculMessagerieDTO.setImpactsMessagerie(new ArrayList<>());
        for(CritereDTO critereDTO : calculMessagerieDTO.getCriteres()) {
            ImpactMessagerieDTO impactMessagerieDTO = referentielClient.getMessagerie(critereDTO.getNomCritere());
            if(impactMessagerieDTO != null) {
                calculMessagerieDTO.addImpactsMessagerieItem(impactMessagerieDTO);
            }
        }

        return calculMessagerieDTO;
    }
}
