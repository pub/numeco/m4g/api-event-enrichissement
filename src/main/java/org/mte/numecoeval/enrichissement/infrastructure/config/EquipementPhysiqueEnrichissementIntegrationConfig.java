package org.mte.numecoeval.enrichissement.infrastructure.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.NewTopic;
import org.mte.numecoeval.topic.data.CalculEquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.expression.ValueExpression;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.outbound.KafkaProducerMessageHandler;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import java.time.format.DateTimeFormatter;
import java.util.UUID;

@Slf4j
@Configuration
@ConditionalOnProperty(
        value = "numecoeval.features.eqp"
)
public class EquipementPhysiqueEnrichissementIntegrationConfig {

    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");

    @Value("${numecoeval.topic.entree.equipementPhysique}")
    String topicEntree;

    @Value("${numecoeval.topic.calcul.equipementPhysique}")
    String topicCalcul;

    @Bean
    public NewTopic equipementPhysiqueTopicKafkaEntree() {
        return new NewTopic(topicEntree, 1, (short) 1);
    }

    @Bean
    public NewTopic equipementPhysiqueTopicKafkaSortieCalcul() {
        return new NewTopic(topicCalcul, 1, (short) 1);
    }

    @Bean
    public MessageChannel equipementPhysiqueEnrichissement() {
        return new DirectChannel();
    }

    @Bean
    public MessageChannel equipementPhysiqueOutputChannelToCalcul() {
        return new DirectChannel();
    }

    // Entrée
    @Bean
    public KafkaMessageDrivenChannelAdapter<String, EquipementPhysiqueDTO> equipementPhysiqueKafkaAdapter(KafkaMessageListenerContainer<String, EquipementPhysiqueDTO> container) {
        var kafkaMessageDrivenChannelAdapter =
                new KafkaMessageDrivenChannelAdapter<>(container, KafkaMessageDrivenChannelAdapter.ListenerMode.record);
        kafkaMessageDrivenChannelAdapter.setOutputChannelName("equipementPhysiqueEnrichissement");
        return kafkaMessageDrivenChannelAdapter;
    }

    @Bean
    public KafkaMessageListenerContainer<String, EquipementPhysiqueDTO> equipementPhysiqueKafkaContainer(ConsumerFactory<String, EquipementPhysiqueDTO> consumerFactory) {
        ContainerProperties properties = new ContainerProperties(this.topicEntree);
        // set more properties
        return new KafkaMessageListenerContainer<>(consumerFactory, properties);
    }

    // Traitement voir classe EnrichissementEquipementPhysiqueService

    // Sortie
    @Bean
    @ServiceActivator(inputChannel = "equipementPhysiqueOutputChannelToCalcul")
    public MessageHandler equipementPhysiqueCalculHandler(KafkaTemplate<String, CalculEquipementPhysiqueDTO> kafkaTemplate) {
        KafkaProducerMessageHandler<String, CalculEquipementPhysiqueDTO> handler =
                new KafkaProducerMessageHandler<>(kafkaTemplate);
        handler.setMessageKeyExpression(new LiteralExpression(UUID.randomUUID().toString()));
        handler.setTopicExpression(new ValueExpression<>(topicCalcul));
        return handler;
    }

}
